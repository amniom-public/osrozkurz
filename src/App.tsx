import { useState, useCallback } from "react";

import CompleteStrategy from "./sections/CompleteStrategy/CompleteStrategy";
import Gates from "./sections/Gates/Gates";
import Header from "./sections/Header/Header";
import Order from "./sections/Order/Order";
import Pain from "./sections/Pain/Pain";
import Path from "./sections/Path/Path";
import Questions from "./sections/Questions/Questions";
import Popup from "./UI/Popup/Popup";

export const App = () => {
  const [popupText, setPopupText] = useState<string | null>(null);
  const [popupError, setPopupError] = useState<boolean>(false);

  const popup = useCallback((text: string, isError: boolean = false) => {
    setPopupError(isError);
    setPopupText(text);
  }, []);

  return (
    <div style={{overflow: 'hidden'}}>
      <Header></Header>
      <Path></Path>
      <CompleteStrategy></CompleteStrategy>
      <Pain></Pain>
      <Gates></Gates>
      <Order popup={popup}></Order>
      <Questions></Questions>
      <div className="Footnote">
        <a target="_blank" href="GDPR.pdf" className="Footnote__a">Ochrana osobních údajů</a> | <a target="_blank" className="Footnote__a" href="https://www.amniomweb.cz">© AmnioM Web s.r.o. </a> (IČO: 11667052) | <a target="_blank" className="Footnote__a" href="https://www.amniomweb.cz">Vytvořil AmnioM Web</a>
      </div>
      {popupText && <Popup isError={popupError} onDismiss={() => {setPopupText(null);setPopupError(false)}} text={popupText} ></Popup>}
    </div>
  );
};
