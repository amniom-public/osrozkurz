import React from 'react';

import './Path.scss';
import journey_img from '../../assets/journey.png';
import long_path_img from '../../assets/long_path.png';
import { Heading3 } from '../../UI/Heading3/Heading3';
import { Paragraph } from '../../UI/Paragraph/Paragraph';

interface PathProps {

}

const Path: React.FC<PathProps> = () => {
    return <div className="Path">
        <img className="Path__img" src={journey_img}></img>
        <div className='Path__intro'>
            <Heading3 className="Path__heading3">jediná cesta</Heading3>
            <Paragraph>Ty emoce, co nechci. Ta zoufalost. Ten strach. Ve svým světě, ve svý představě, je líp. Chci změnu z života, co žiju, do života, co chci žít.</Paragraph>
            <Paragraph>Nikdo mi nepomůže. To už nečekám. Jsem v tom sám, jako vždycky. Jen pár lidí tu cítí podobný pocity. Ví, jaký to je.</Paragraph>
            <Paragraph>Potřebuju strategii.</Paragraph>
            <Heading3 className="Path__heading3">Ta Strategie</Heading3>
            <Paragraph>Byla to zatraceně dlouhá cesta. Od chvíle, co jsem zjistil, že potřebuju strategii, až do dneška. Deset let. Polovina mého života.</Paragraph>
            <Paragraph>V jedenácti mi začlo docházet, že jsem jinej. Nechápal jsem spolužáky a oni se se mnou nechtěli bavit. Divnej, škaredej, utlačenej.</Paragraph>
            <Paragraph>V 16 jsem nevěřil, že zvládnu přežít další rok. Rok pokračující samoty, frustrace, bezmocnosti. Další rok života beze smyslu.</Paragraph>
            <Paragraph>Dneska mi je jednadvacet. A strategii konečně mám.</Paragraph>
            <Paragraph>Pořád nejsem normální. Z divnosti je jedinečnost. Nejsem sám. Lidi ze mě cítí něco speciálního. Zajímám je. Mají mě rádi. Někteří mě obdivují.</Paragraph>
            <Paragraph>Ten, kdo používá strategii, nikdy nedorovná ostatní. Předběhne je. Teleportuje se daleko před ně. </Paragraph>
            <Paragraph>Jen ten, kdo trpí a dokáže najít cestu, bude mít otevřené dveře nekonečným možnostem. Mnohem větším, než jaké mají ti “normální”. Ti, co si nikdy neprošli peklem a nikdy na sobě nemuseli pracovat.</Paragraph>
            <Heading3 className="Path__heading3">ze dna na vrcholy</Heading3>
            <Paragraph>Strategie je nakonec celkem jasná. Všechno se dá odvodit, všechno dává smysl. Je založená na logice a postupně člověka vezme ze dna na vrchol.</Paragraph>
            <Paragraph>Na jaký vrchol? Na všechny! Celková spokojenost, romantický život, seberealizace, vztahy, láska, vydělávání peněz, sebepoznávání, naplněný život, štěstí, sebevědomí, klid, respekt, svoboda, přijetí a další.</Paragraph>
            <Paragraph>Strategie prolomí kruh beznaděje. Vyřeší problém, ne jen symptomy. Ukážeš ostatním, že nejsi ten, do kterýho si můžou kopnout. Dobře si zapamatují, kdo jsi.</Paragraph>
            <Paragraph>Strategie tě zbaví bolesti, úzkosti, smutku. Povstaneš jako fénix. Silnější, než kdy předtím.</Paragraph>
            <Paragraph> Je to možný? Funguje to? Nikdy jsem nevěřil, že by taková strategie mohla existovat. Když jsem se v úkosti pod peřinou pokoušel nadechnout, nikdy by mě nenapadlo, že budu tak spokojený, úspěšný, milovaný, jako jsem dnes. Je to neuvěřitelný.</Paragraph>
        </div>
        <div className='Path__myPath'>
            <Heading3 className='Path__heading3'>jaká byla moje cesta?</Heading3>
            <iframe className='Path__myPath__video' width="560" height="315" src="https://www.youtube.com/embed/qnyFmKQ1d9c" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
        <div className='Path__solution'>
            <Heading3 className='Path__heading3'>co strategie řeší</Heading3>
            <Paragraph>Schválně jsem si zkusil vypsat všechno, co strategie přinesla a co všechno se s její pomocí vyřešilo... Za všechny ty roky se povedlo tohle všechno a mnohem víc, na co jsem si ani nevzpomněl. Kdybych se mohl vrátit v čase do nejhorší doby, dal bych mladšímu já strategii a řekl mu:</Paragraph>
            <ul>
                <li>Přestaneš se nenávidět</li>
                <li><span>Objevíš</span> sebelásku, sebepřijetí a spokojenost se <span>sebou</span></li>
                <li>Ukončíš stres</li>
                <li><span>Přestaneš se </span>bát lidí <span>a</span> lidi se přestanou bát tebe</li>
                <li><span>Najdeš</span> štěstí</li>
                <li><span>Začneš být</span> rád na světě</li>
                <li><span>Začneš zažívat</span> naplňující interakce</li>
                <li><span>Vybuduješ si</span> dobré vztahy</li>
                <li><span>Začneš žít</span> romantický a milostný život</li>
                <li><span>Naučíš</span> se dotýkat se lidí a doteky přijímat</li>
                <li><span>Přijímat a dávat se naučíš i</span> lásku</li>
                <li><span>Ucítíš</span> vlastní hodnotu <span>a že si</span> zasloužíš to nejlepší</li>
                <li><span>Naučíš se projevovat vlastní</span> jedinečnost</li>
            </ul>
            <Paragraph>Je toho dost? Už jen za tohle to stojí. Ale zdaleka to není všechno.</Paragraph>
            <ul>
                <li>Prolomíš <span>existenční krizi</span> a pocit nesmyslnosti</li>
                <li><span>Lidi tě začnou</span> přijímat takového, jaký jsi</li>
                <li><span>Budeš</span> lepší, než ostatní</li>
                <li>Přestaneš být divný a rozbitý</li>
                <li><span>Budeš mít kolik</span> přátel <span>budeš chtít</span></li>
                <li><span>Budeš mít tolik</span> přítelkyň a milenek <span>kolik budeš chtít</span></li>
                <li><span>Budeš</span> žít na plno <span>tak, jak jsi nikdy nevěřil že budeš</span></li>
                <li><span>Lidi si tě budou</span> vážit, <span>budou tě mít</span> rádi, <span>budou tě</span> repektovat</li>
                <li><span>Lidi budou chtít být</span> kolem tebe, <span>budou se</span> na tebe těšit</li>
                <li><span>Po té cestě budeš umět</span> opravdu pomoct <span>ostatním</span></li>
                <li>Zbavíš se depresí a úzkostí</li>
                <li>Mezi lidmi <span>se budeš cítít</span> komfortně, <span>žádný strach a úzkost</span></li>
                <li><span>Nikdy ti nedojdou</span> témata o čem mluvit</li>
                <li><span>Budeš</span> s lidmi rád</li>
                <li>Ženy <span>o tebe samy</span> budou mít zájem <span>a budeš si</span> moct vybírat</li>
                <li><span>Začneš být</span> vyrovnaný</li>
                <li><span>Už nikdy tě</span> nenapadne se sebepoškozovat <span>nebo si jinak ublížit</span></li>
                <li>Zbavíš se <span>pocitu</span> beznaděje</li>
                <li><span>Budeš</span> prožívat naplněný život</li>
                <li><span>Bude ti</span> v realitě ještě líp, jak ve vlastních představách</li>
                <li><span>Budeš mít</span> rád emoce <span>a budeš</span> je ovládat <span>podle sebe</span></li>
            </ul>
            <Paragraph>Teď to je všechno? Pořád není.</Paragraph>
            <ul>
                <li>Nebude tě trápit, co si myslí ostatní</li>
                <li>Zbavíš se stresu <span>nejen dlouhodobě, ale</span> i akutních záchvatů úzkosti</li>
                <li><span>Začneš</span> přitahovat správné lidi</li>
                <li><span>Staneš se</span> vyspělým mužem</li>
                <li><span>Ucítíš</span> respekt sám k sobě</li>
                <li><span>Ucítíš</span> sebevědomí</li>
                <li><span>Budeš žít v</span> pocitu svobody</li>
                <li><span>Naučíš se</span> autenticky projevovat</li>
                <li><span>Získáš</span> všeobecnou spokojenost</li>
                <li>Ukážeš ostatním, co dokážeš</li>
                <li>Vezmeš si všechno, co ti kdy chybělo</li>
                <li><span>Při usínání se budeš</span> těšit na další den</li>
                <li><span>Ucítítš vlastní</span> dostatečnost</li>
                <li><span>Poznáš</span> maják, <span>co tě kdykoli</span> vyvádí ze tmy</li>
                <li>Žádná další bolest <span>a ničivá úzkost</span></li>
                <li>Ostatní tě přestanou vnímat jako objekt</li>
                <li>Přestaneš sabotovat sám sebe</li>
                <li>Prolomíš kruh, <span>co tě drží dole</span></li>
                <li>Zpracuješ minulost <span>a</span> osvobodíš se <span>od ní</span></li>
                <li>Z hendikepu uděláš výhodu <span>a</span> všechny předběhneš</li>
                <li>Vyřešíš <span>ty</span> nejhlubší problémy, <span>ne jen symptomy</span></li>
                <li><span>Ostatní tě začnou</span> brát vážně</li>
                <li><span>Lidi si tě budou</span> cenit</li>
            </ul>
            <Paragraph>Už všechno? Určitě ne. Ale už se mi nechce vymýšlet další body.</Paragraph>
        </div>
        <div className='Path__longPath'>
            <Heading3 className='Path__heading3'>je to dlouhá cesta</Heading3>
            <img className="Path__long_path_img" src={long_path_img}></img>
            <Paragraph>
                Jak jsem psal, asi deset let práce na sobě. Nevím, kdo přesně jsi, ale soucítím s tebou a přeji si, aby jsi taky mohl žít na plno.    
            </Paragraph>
            <Paragraph>Asi před třemi lety jsem si začal hrát s myšlenkou pomáhání lidem, jako jsem byl kdysi já. Tenkrát jsem ale ještě neměl tolik co učit, protože sám jsem měl hodně práce a objevování před sebou.</Paragraph>
            <Paragraph> Dneska, když mám, co jsem vždycky chtěl, to chci předat. Jestli tady má po mě zůstat jedna věc, tak je to strategie. Cesta ze dna života a ze dna společnosti k naplněnosti a štěstí.</Paragraph>
            <Paragraph>Proč? Protože to, co mně trvalo objevovat tak dlouho, zkoušet různé techniky metodou pokus-omyl, zjišťovat, které přístupy vedou jen k frustraci a vyhoření, otevře ostatním cestu, kterou zvládnou za zlomek času.</Paragraph>
        </div>
    </div>
};

export default Path;