import React from 'react';

import './Gates.scss';
import { Heading1 } from '../../UI/Heading1/Heading1';
import { Heading2 } from '../../UI/Heading2/Heading2';
import { Paragraph } from '../../UI/Paragraph/Paragraph';

import crouching_person_img from '../../assets/crouching_person.svg';
import curtain_img from '../../assets/curtain.svg';
import { Heading3 } from '../../UI/Heading3/Heading3';

interface GatesProps {

}

const Gates: React.FC<GatesProps> = () => {
    return <div className="Gates">
        <Heading3 className='Gates__heading3'>před branami života, co stojí za to žít</Heading3>
        <Paragraph>
            Jsem připravený celou strategii zveřejnit. Na jejím natočení začínám pracovat, když mi ukážeš, že o ni máš zájem.</Paragraph>
        <Paragraph>
        Právě teď dávám ven tuhle stránku a ty jsi jeden z prvních stovek lidí, kteří ji vidí. Jsi tak jeden z prvních, kteří strategii poznají a začnou ji používat.
        </Paragraph>
        <Paragraph>Na mail, který napíšeš dole, ti pošlu strategii v předprodeji hned jak začnou vycházet první videa, ať se co nejrychleji dostaneš na lepší místo.</Paragraph>
        <Paragraph>Můžeš i ovlivnit, jakým způsobem strategii natáčím. Takhle na začátku budu muset získat hodně zpětné vazby, abych strategii natočil co nejlepším způsobem.</Paragraph>
        <Paragraph>Protože jsi tu tak brzo, stihneš nízkou cenu v předprodeji a můžeš na ni mít ještě 33% slevu. Dostaneš ji na mail, co vyplníš dole.</Paragraph>
        <Paragraph>A klidně ti dám i ještě nižší cenu. To pak ale potřebuji tvou pomoc. Když pro mě vyplníš dotazník kolem zpracování strategie, dám ti strategii za polovinu.</Paragraph>
        <Paragraph>Nakonec je možnost cca 2h online rozhovoru (i bez webkamery) se mnou, kde se tě zeptám víc detailně, jak si představuješ, že bude strategie zpracovaná. Za to máš celou strategii skoro zdarma (90% slevu) hned, jak začne vycházet.</Paragraph>
        <Heading3 className='Gates__heading3'>je čas</Heading3>
        <Paragraph>Nemáš, co ztratit. Čím později začneš, tím bude cesta složitější.</Paragraph>
        <Paragraph>Celá <span>strategie je tvoje</span>. Kdykoli se můžeš <span>zeptat na radu ostatních</span> na stejné cestě ve FB skupině a slibuji ti, že ti v průběhu tvé cesty sám odpovím aspoň na tři tvoje otázky přámo na mě.</Paragraph>
        <Paragraph>Do 60 dnů od zaplacení (koupit bude možné až se spustí předprodej) - když budeš mít pocit, že ti strategie nepomáhá, vrátím ti plnou cenu.</Paragraph>
        <Paragraph>Jakmile odešleš formulář, <span>na předprodej ti pošlu slevu</span> podle tvého výběru a případně se domluvíme na čas rozhovoru.</Paragraph>
        <Paragraph>Je čas si to vzít. Představ si to.</Paragraph>
        <Paragraph className='Gates__mainP'>Máš upřímné naplňující vztahy, které jsi vždycky chtěl. Nejlepšího kamaráda a kruh dobrých přátel. Přítelkyni, co tě má ráda, ať se děje cokoli. Jsi vzorem pro ostatní. Už pro ně nejsi looser ani slaboch. Jsi sám sebou. Říkáš, co si myslíš. Sám si vybíráš, kdo je správný člověk, se kterým stojí za to se bavit. Bereš si to, co ti vždycky chybělo.</Paragraph>
        <Heading3 className='Gates__heading3'>Před sebou máš</Heading3>
        <Paragraph className='Gates__smallP'>Více než sto videí a spoustu transformačních cvičení, co ti změní život</Paragraph>
        <Heading3 className='Gates__heading3'>K tomu zdarma</Heading3>
        <Paragraph className='Gates__smallP Gates__smallP--list'>
            ○ 3 otázky přímo na mě<br></br>
            ○ Přístup do vzájemně se podporující komunity<br></br>
            ○ Slevu na předprodej<br></br>
        </Paragraph>
        <Paragraph className='Gates__smallP'>A slib vrácení peněz do 60 dnů, když to nebude pomáhat.</Paragraph>
        <Paragraph className='Gates__smallP'>(Strategii si zatím není možné koupit, to půjde až v předprodeji, když bude o strategii dostatečný zájem. Zájem je ale potřeba projevit teď)</Paragraph>
    </div>
};

export default Gates;