import React from 'react';

import './Header.scss';
import { Heading1 } from '../../UI/Heading1/Heading1';
import { Heading2 } from '../../UI/Heading2/Heading2';
import { Paragraph } from '../../UI/Paragraph/Paragraph';

import crouching_person_img from '../../assets/crouching_person.svg';
import curtain_img from '../../assets/curtain.svg';

interface HeaderProps {

}

const Header: React.FC<HeaderProps> = () => {
    return <div className="Header">
        <Heading1 className="Header__heading">
            Úzkost, smutek, samota, beznaděj... Tohle teď končí. Vyřešíme to.
        </Heading1>
        <Heading2 className="Header__heading2">
            <img className="Header__img" src={crouching_person_img}></img>
            Pro mladé muže, jako ty, kteří <br></br>nezapadají.
        </Heading2>
        <Paragraph className='Header__p'>
        Musím to prožívat? Musím být ten divnej, se kterým se nikdo nechce bavit? Nechápu lidi. Jsem zmatenej a nevím co s tím dělat. Asi jsem se takovej narodil. Divnej, nechtěnej, nepochopenej, rozbitej. Opuštěnej. Je se mnou něco špatně. Vžycky bylo.
        </Paragraph>
        <Paragraph className='Header__p'>
        Chci jenom utýct z týhle reality. Do světa, kde je všechno v pořádku. Kde to tak nebolí. Kde mám naději. Kde mě lidi mají rádi. Kde můžu být sám sebou a nikdo mě nebude soudit. Nikdo se na mě nebude divně dívat. Smát se mi. Pomlouvat mě.
        </Paragraph>
        <img className="Header__curtain Header__curtain--left" src={curtain_img}></img>
        <img className="Header__curtain Header__curtain--right" src={curtain_img}></img>
    </div>
};

export default Header;