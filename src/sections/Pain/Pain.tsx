import React from 'react';

import './Pain.scss';
import { Paragraph } from '../../UI/Paragraph/Paragraph';

import pain_img from '../../assets/pain.svg';
import { Heading3 } from '../../UI/Heading3/Heading3';

interface PainProps {

}

const Pain: React.FC<PainProps> = () => {
    return <div className="Pain">
        <img src={pain_img} className='Pain__img'></img>
        <Heading3 className='Pain__heading3'>Některé lidi bolí žít celý život</Heading3>
        <Paragraph>
            Ty jsi naštěstí objevil cestu. Ale někteří ji neobjeví nikdy. Nebo se nikdy neodhodlají udělat první krok. Je to jedna z nejsmutnějších představ. Člověk tu žije jenom jednou a někteří celý svůj život tráví v bolesti, smutku, úzkostech, zmatenosti, nenaplněnosti...  </Paragraph>
        <Paragraph>Co se po letech takových emocí stane je buď ukončení života, nebo doživotní zahořknutí. Každý den další úzkosti, hlubší deprese. Žádné světlo na konci tunelu.</Paragraph>
        <Paragraph>Ti, kteří nic neudělají, dál prožívají pocity nedostatečnosti, lidi je soudí a nemají je rádi, mají do smrti špatné vztahy, třeba si ani nenajdou dobrou partnerku.</Paragraph>
        <Paragraph>Všechno tak zůstává “při starým”, což je hrozně děsivá varianta. Nezažívají opravdové pocity lásky, upřímné něžné doteky, lidi se jich straní. Dál je ostatní nechápou, přijdou jim divní.</Paragraph>
        <Paragraph>Ostatní je nerespektují, berou je spíš jako věc než člověka. A na smrtelné posteli si takoví pak vyčítají, že se s tím nikdy nic nepokusili udělat. Že promarnili svou jedinou možnost žít život.</Paragraph>
        <Paragraph>Postupně ztrácí kamarády, jsou pro ostatní looseři. Nikdo si nevzpomene na jejich narozeniny, nikdo je nebere vážně, smějí se jim.</Paragraph>
        <Paragraph>Jenom o tom psát je strašný. Nejde jen o to, že by celý život prožívali tyhle emoce, ale často se takhle uvrhnou do mnohem větších problémů. Můžou pak skončit odstrčení a bez peněz.</Paragraph>
        <Paragraph>Nenávidí sami sebe celý život, protože se nikdy nepoznají a nepřijmou. Jsou otroky beznaděje. Nikdy nezažijí, jaké to je, se autenticky projevit. Jaké to je být přijímaný takoví, jací jsou.</Paragraph>
        <Paragraph>Zůstávají i ty menší, ale pořád strašně frustrující věci. Třeba neschopnost udržet kvalitní konverzaci. Třeba neví, o čem se bavit s ostatníma, a jen tak to nikdy nezjistí. Takže si nikdy nebudou moct užít čas s ostatníma.</Paragraph>
        <Paragraph>Celý život se cítí nenaplnění. Celý život v sobě dusí emoce jak v papyňáku. Ze všeho toho stresu si můžou způsobit třeba i rakovinu. Což mimochodem způsobuje i nedostatek lásky a dotyku.</Paragraph>
        <Paragraph>Cítí se zbytečně, bez vnitřního klidu. A ubližují tak i lidem, co je ještě mají rádi.</Paragraph>
        <Paragraph>Přitom stačilo jen tenkrát, když ještě věci šly změnit, když ještě cítili naději, to všechno vzít do svých rukou.</Paragraph>
        <Heading3 className='Pain__heading3'>
            teď, nebo to zatím nechat být?
        </Heading3>
        <Paragraph><span>Čím je člověk starší, tím těžší je změnu udělat.</span> V jakémkoli věku se dá udělat první krok, ale je to s věkem těžší. Kdybys udělal první krok třeba za půl roku, bude ti cesta trvat o dost dýl, než když uděláš první krok dneska.
        </Paragraph>
        <Paragraph>
            Je to kvůli plasticitě mozku. Mozek se dokáže skvěle učit, nastavovat, vyvíjet. Hlavně v dětství. Čím jsme starší, tím je to obtížnější, protože se nám snižuje právě ta plasticita. To znamená, že neurony dělají ve vyšším věku méně nových propojení. A propojení, co už existují, jsou s věkem čím dal silnější. A když nám ty propojení způsobují třeba smutek, je to problém. <span>Proto čím dřív, tím líp.</span>
        </Paragraph>
    </div>
};

export default Pain;