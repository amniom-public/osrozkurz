import React from 'react';
import { Heading3 } from '../../UI/Heading3/Heading3';
import { Paragraph } from '../../UI/Paragraph/Paragraph';

import './Questions.scss';


interface QuestionsProps {

}

const Questions: React.FC<QuestionsProps> = () => {
    return <div className="Questions">
        <Heading3 className='Questions__heading3'>máš otázky?</Heading3>
        <h5 className='Questions__heading5'>Stojí to za to? Bude to fungovat?</h5>
        <Paragraph>Bude to fungovat, ale má to podmínky.</Paragraph>
        <Paragraph>Podmínky na mé straně jsou, že strategii zpracuju nejlíp, jak to půjde. A proto potřebuju tvoji pomoc. Teď, ještě před spuštěním, moc pomůže, když vyplníš dotazník nebo si se mnou zavoláš o formě zpracování. A jakmile vyjdou první videa, budu potřebovat zpětnou vazbu, abych to mohl udělat ještě líp.</Paragraph>
        <Paragraph>Podmínky na tvé straně jsou, že budeš pozorný a otevřený celé strategii jak po stránce teorie, tak po stránce transformačních cvičení. Jedno bez druhého nikdy nemůže fungovat.</Paragraph>
        <h5 className='Questions__heading5'>Jak to bude probíhat?</h5>
        <Paragraph>Zjistíš, jak funguješ ty, jak fungují ostatní, jak ovlivnit, co se děje. A nejen že to zjitíš, ale budeš trénovat všechno, co se naučíš, tak dlouho, dokud to nezačneš dělat automaticky. Pak se posuneš k dalšímu kroku a tak pořád dokola, dokud se nezbavíš bolesti, kterou nechceš, a dokud nebudeš mít cokoli dalšího, co chceš.</Paragraph>
        <h5 className="Questions__heading5">Problém je v ostatních, ne ve mě</h5>
        <Paragraph>Může to tak být. Skvělá zpráva je, že ostatní můžeme měnit. Jejich chování k nám měníme hlavně tím, jak my se chováme k nim. A to jde naučit.</Paragraph>
        <h5 className='Questions__heading5'>Stejně je to se mnou marný...</h5>
        <Paragraph>Znám lidi, se kterýma to je opravdu marný. Ať děláš cokoli, nikdy jim nepomůžeš. Říkají, že chtějí, aby to bylo lepší, ale nikdy pro to neudělají nic.</Paragraph>
        <Paragraph>Když jsem začal strategii objevovat, sám jsem si myslel, že jsem marnej. Představa, že budu někdy tak daleko, jako teď, byla úplně mimo. Přesto jsem nepřestával dělat malý kroky dopředu kdykoli jsem mohl.</Paragraph>
        <Paragraph>Najednou se dívám za sebe a skoro tomu nevěřím. To, co teď mám, je mnohem víc, než jsem kdy chtěl. Chtěl jsem být alespoň schopný přežívat, najednou žiju.</Paragraph>
        <Paragraph>One step at a time. Teď ti stačí poslat formulář. Další kroky neřeš, to se vyřeší později.</Paragraph>
        <h5 className='Questions__heading5'>Proč to děláš?</h5>
        <Paragraph>Život je příliš krátký, abychom v něm trpěli. Vím, jaký to je dole, kde se ani nechce žít. Představa že jsou ostatní, co trpí tak, jako jsem trpěl já, mě děsí.</Paragraph>
        <h5 className="Questions__heading5">Neudělám si to po svým líp?</h5>
        <Paragraph>Kdyby ses roky snažil, nikdy nepřestával hledat, procházel mrzačící bolestí a vždycky se zvednul, jako robot, a prostě šel dál, zvládnul bys to.</Paragraph>
        <Paragraph>I s celou strategií tě bude cesta někdy bolet. Budeš muset být silný. Připravený vstát i když bude všechno černý.</Paragraph>
        <Paragraph>Strategii ber jako cheat. Nakonec je na tobě, co z ní použiješ, co se z ní naučíš. Dávám ti maximum co můžu. Budeš mít v rukách to, co já neměl, co ti ušetří několik let studia, vymýšlení a experimentování pokus-omyl.</Paragraph>
        <p className="Questions__contact">kontakt: yourguide@notillum.com</p>
    </div>
};

export default Questions;