import React from 'react';
import { Heading3 } from '../../UI/Heading3/Heading3';
import { Paragraph } from '../../UI/Paragraph/Paragraph';

import buyback_img from '../../assets/buyback.svg';
import questionmark_img from '../../assets/questionmark.svg';
import support_img from '../../assets/support.svg';
import video_img from '../../assets/video.svg';

import './CompleteStrategy.scss';
import { CTA } from '../../UI/CTA/CTA';

interface CompleteStrategyProps {

}

const CompleteStrategy: React.FC<CompleteStrategyProps> = () => {
    return <div className="CompleteStrategy">
        <Heading3>Kompletní Praktická Strategie</Heading3>
        <Paragraph>
            Všechno to, co jsem psal výše - to přijetí, spokojenost, konec utrpení, vztahy, láska... je tady. Kompletní strategie podaná maximálně prakticky. Každou teorii podkládám nějakým cvičením, které jsem sám používal. Tohle je moc důležitá věc. Znát strategii je základ. Pak je potřeba ji žít. Můžeš být šachový velmistr, ale pokud neuděláš tah, prohraješ.
        </Paragraph>
        <div className='CompleteStrategy__features'>
            <div className="CompleteStrategy__feature">
                <div className='CompleteStrategy__feature__left'>
                    <img src={video_img} className='CompleteStrategy__feature__img'></img>
                </div>
                <div className='CompleteStrategy__feature__right'>
                    <h4 className='CompleteStrategy__feature__headline'>Více než sto videí a praktická cvičení</h4>
                    <p className='CompleteStrategy__feature__p'>Více než sto videí přináší tohle všechno. A kdykoli přijdu na něco nového, další videa a cvičení přidávám.</p>
                </div>
            </div>
            <div className="CompleteStrategy__feature">
                <div className='CompleteStrategy__feature__left'>
                    <img src={questionmark_img} className='CompleteStrategy__feature__img'></img>
                </div>
                <div className='CompleteStrategy__feature__right'>
                    <h4 className='CompleteStrategy__feature__headline'>Otázky přímo na mě</h4>
                    <p className='CompleteStrategy__feature__p'>E-mail yourguide@notillum.com nechávám otevřený otázkám přímo na mě. Zdarma ke strategii jsem chtěl dát jednu otázku, abych to stíhal. Ale říkám si, že jedna je málo. Tak slibuju, že zdarma odpovím ne na jednu, ale aspoň na tři tvoje otázky.</p>
                </div>
            </div>
            <div className="CompleteStrategy__feature">
                <div className='CompleteStrategy__feature__left'>
                    <img src={support_img} className='CompleteStrategy__feature__img'></img>
                </div>
                <div className='CompleteStrategy__feature__right'>
                    <h4 className='CompleteStrategy__feature__headline'>Podpora od ostatních na stejné cestě</h4>
                    <p className='CompleteStrategy__feature__p'>A dál, protože lidi mezi sebou si taky dokážou hodně pomoct, tě, když budeš chtít, přidám do FB skupiny, kde budou další kluci a mladí muži, co chtějí lepší život. Můžeš tam sdílet svoje úspěchy, ptát se ostatních, jak by co řešili, a sám pomáhat ostatním v tom, co už sám zvládáš. Já tam jsem taky aktivní, takže tam můžeš narazit i přímo na mě.</p>
                </div>
            </div>
            <div className="CompleteStrategy__feature">
                <div className='CompleteStrategy__feature__left'>
                    <img src={buyback_img} className='CompleteStrategy__feature__img'></img>
                </div>
                <div className='CompleteStrategy__feature__right'>
                    <h4 className='CompleteStrategy__feature__headline'>Chci ti vrátit peníze, když to nepomůže</h4>
                    <p className='CompleteStrategy__feature__p'>Chci si být jistý, že opravdu pomáhám. Všem, kteří budou mít pocit, že jim strategie z nějakého důvodu nepomáhá, chci vrátit peníze. Stačí mi napsat na yourguide@notillum.com. Abych se sám chránil, platí to na 60 dnů od nakoupení - snad dost času zjistit, jestli ti strategie funguje.</p>
                </div>
            </div>
        </div>
        <CTA className="CompleteStrategy__CTA" onClick={() => {
            gtag && gtag("event", "cta_pressed", { id: "2" })
            const order = document.getElementById('order');
            order?.scrollIntoView({behavior: 'smooth'})
        }}>
            Jsem připravený (tlačítko tě vezme ke strategii)
        </CTA>
    </div>
};

export default CompleteStrategy;