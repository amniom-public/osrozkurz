import React, { useState, useCallback } from 'react';
import axios from 'axios';

import './Order.scss';
import path_img from '../../assets/long_path.png';
import { CTA } from '../../UI/CTA/CTA';

interface OrderProps {
    popup: (text: string, isError?: boolean) => void
}

const Order: React.FC<OrderProps> = ({ popup }) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [discount, setDiscount] = useState(0);
    const [FBChecked, setFBChecked] = useState(false);
    const [agreementChecked, setAgreementChecked] = useState(false);
    const [FBProfileURL, setFBProfileURL] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const onCTAClicked = useCallback(() => {
        if (isLoading) {
            return;
        }
        if (!agreementChecked) {
            popup("Označ, prosím, že souhlasíš se zpracováním osobních údajů", true)
            return;
        }
        setIsLoading(true);
        console.log('clicking the ca', process.env.REACT_APP_BACKEND_URL)
        const discountMapping = ['BASIC', 'ANSWERS', 'TALK'];
        gtag && gtag("event", "cta_pressed", { id: "1" })
        axios
            .post(process.env.REACT_APP_BACKEND_URL! + "/interested", {
                firstName: name,
                email: email,
                discountType: discountMapping[discount],
                FBProfileURL: FBProfileURL
            })
            .then(() => {
                setIsLoading(false);
                setEmail('');
                setName('');
                setFBProfileURL('');
                popup("Povedlo se! Zkontroluj si mail :)");
                gtag && gtag("event", "generate_lead", { currency: "CZK", value: 1 });
                fbq && fbq('track', 'Lead', { currency: "CZK", value: 1 });
                ttq && ttq.track('SubmitForm');
            })
            .catch(e => {
                const err = e.response.data.err as string;
                setIsLoading(false);
                if (err === "email") {
                    popup(
                        "Zkontroluj prosím, zda jsi správně zadal email",
                        true
                    );
                } else if (err === "firstName") {
                    popup(
                        "Zkontroluj prosím, zda jsi zadal jméno",
                        true
                    );
                } else if (err === "discountType") {
                    popup(
                        "vyber prosím typ slevy",
                        true
                    );
                } else if (err === "FBProfile") {
                    popup(
                        "Zkontroluj prosím, zda jsi správně zadal FB Profile URL",
                        true
                    );
                } else {
                    popup(
                        "Nastala náhodná chyba. Zkus to později, nebo mi napiš",
                        true
                    );
                }
            });
    }, [isLoading, setIsLoading, name, email, discount, FBProfileURL, agreementChecked, setEmail, setName, setFBProfileURL]);

    return <div id="order" className="Order">
        <img className='Order__img' src={path_img}></img>
        <div className='Order__form'>
            <div className='Order__textInput'>
                <label className='Order__textInput__label' htmlFor="name"><span>*</span>Tvoje křestní jméno</label>
                <input value={name} onChange={(e) => { setName(e.target.value) }} id="name" className='Order__textInput__input'></input>
            </div>
            <div className="Order__textInput">
                <label className='Order__textInput__label' htmlFor="email"><span>*</span>E-mail</label>
                <input value={email} onChange={(e) => { setEmail(e.target.value) }} id="email" className='Order__textInput__input'></input>
            </div>
            <div className='Order__discount'>
                <p className='Order__discount__p'>Zatím můžeš využít tyhle slevy, později už nebudou:</p>
                <div className='Order__radioInput'>
                    <label className={['Order__radioInput__label', discount === 0 ? 'Order__radioInput__label--checked' : ''].join(' ')}>
                        <input onChange={(e) => { setDiscount(0) }} name='discount' className='Order__radioInput__input' type="radio"></input>
                    </label>
                    <div className='Order__radioInput__text'>chci 33% slevu na předprodejní cenu</div>
                </div>
                <div className='Order__radioInput'>
                    <label className={['Order__radioInput__label', discount === 1 ? 'Order__radioInput__label--checked' : ''].join(' ')}>
                        <input onChange={(e) => { setDiscount(1) }} name='discount' className='Order__radioInput__input' type="radio"></input>
                    </label>
                    <div className='Order__radioInput__text'>chci pomoct vyplněním otázek o zpracování strategie za 50% slevu na předprodejní cenu</div>
                </div>
                <div className='Order__radioInput'>
                    <label className={['Order__radioInput__label', discount === 2 ? 'Order__radioInput__label--checked' : ''].join(' ')}>
                        <input onChange={(e) => { setDiscount(2) }} name='discount' className='Order__radioInput__input' type="radio"></input>
                    </label>
                    <div className='Order__radioInput__text'>chci pomoct cca 2h rozhovorem o zpracování strategie za 90% slevu na předprodejní cenu</div>
                </div>
            </div>
            <div className='Order__FB'>
                <div className='Order__checkboxInput'>
                    <label className={['Order__checkboxInput__label', FBChecked ? 'Order__checkboxInput__label--checked' : ''].join(' ')}>
                        <input type='checkbox' onChange={(e) => { setFBChecked(v => !v) }} className='Order__checkboxInput__input'></input>
                    </label>
                    <p className='Order__checkboxInput__text'>Chci přidat do FB komunity</p>
                </div>
                {
                    FBChecked ? (<div className='Order__textInput'>
                        <label className='Order__textInput__label' htmlFor="fbLink">URL odkaz na FB profil</label>
                        <input value={FBProfileURL} onChange={(e) => { setFBProfileURL(e.target.value) }} id="fbLink" className='Order__textInput__input'></input>
                    </div>) : null
                }
            </div>
            <div className='Order__checkboxInput'>
                <label className={['Order__checkboxInput__label', agreementChecked ? 'Order__checkboxInput__label--checked' : ''].join(' ')}>
                    <input type='checkbox' onChange={(e) => { setAgreementChecked(v => !v) }} className='Order__checkboxInput__input'></input>
                </label>
                <p className='Order__checkboxInput__text'><span>*</span>Souhlasím se <a target="_blank" href="GDPR.pdf">zpracováním osobních údajů</a></p>
            </div>
            <CTA onClick={onCTAClicked} className='Order__CTA'>{isLoading ? 'Počkej prosím chvíli ...' : 'CHCI ZMĚNU, není co ztratit (na mail ti pošlu slevu a info, jakmile začne předprodej)'}</CTA>
            <p className='Order__endNote'>
                možnost brzy končí - spustí se předprodej, nebo, jestli nebude zájem, strategii bohužel nevydám
            </p>
        </div>
    </div>
};

export default Order;